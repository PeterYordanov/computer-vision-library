#include "qrcodescanner.hpp"

QRCodeScanner::QRCodeScanner()
{

}

cv::Mat QRCodeScanner::apply(const cv::Mat& image)
{
    cv::QRCodeDetector detector;
    cv::Mat boundingBox;
    cv::Mat rectifiedImage;

    std::string data = detector.detectAndDecode(image, boundingBox, rectifiedImage);

    if(data.length() > 0) {
        std::cout << "Decoded Data: " << data << std::endl;

        int n = boundingBox.rows;
        for(int i = 0 ; i < n ; i++) {
            cv::line(image, cv::Point2i(boundingBox.at<float>(i, 0), boundingBox.at<float>(i, 1)),
                     cv::Point2i(boundingBox.at<float>((i + 1) % n, 0), boundingBox.at<float>((i + 1) % n, 1)),
                     cv::Scalar(0, 255, 0), 3);
        }

        return image;
    }

    return image;
}
