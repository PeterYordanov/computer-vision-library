#ifndef CONVEXHULL_HPP
#define CONVEXHULL_HPP

#include "basealgorithm.hpp"

class ConvexHull : public BaseAlgorithm
{
public:
    ConvexHull() = default;

    cv::Mat apply(const cv::Mat&);
};

#endif // CONVEXHULL_HPP
