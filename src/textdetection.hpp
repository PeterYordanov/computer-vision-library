#ifndef TEXTDETECTION_HPP
#define TEXTDETECTION_HPP

#include "basealgorithm.hpp"

class TextDetection : public BaseAlgorithm
{
public:
    TextDetection();

    cv::Mat apply(const cv::Mat&);

private:
    void decode(const cv::Mat& scores,
                const cv::Mat& geometry,
                float scoreThreshold,
                std::vector<cv::RotatedRect>& detections,
                std::vector<float>& confidences);

    cv::dnn::Net m_net;
    float m_confidenceThreshold = 0.5;
    float n_nmsThreshold = 0.4;
    int m_width = 320;
    int m_height = 320;

    std::vector<cv::String>* m_outputLayers;
    cv::Mat blob;

};

#endif // TEXTDETECTION_HPP
