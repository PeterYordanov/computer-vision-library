#ifndef QRCODESCANNER_HPP
#define QRCODESCANNER_HPP

#include "basealgorithm.hpp"

class QRCodeScanner : public BaseAlgorithm
{
public:
    QRCodeScanner();

    cv::Mat apply(const cv::Mat&);

};

#endif // QRCODESCANNER_HPP
