#ifndef BASEALGORITHM_HPP
#define BASEALGORITHM_HPP

#include <opencv2/opencv.hpp>

class BaseAlgorithm
{
public:
    virtual cv::Mat apply(const cv::Mat&) = 0;
};

#endif // BASEALGORITHM_HPP
