#include "textdetection.hpp"

TextDetection::TextDetection()
{
    m_net = cv::dnn::readNet("deps/models/frozen_east_text_detection.pb");

    m_outputLayers = new std::vector<cv::String>(2);
    (*m_outputLayers)[0] = "feature_fusion/Conv_7/Sigmoid";
    (*m_outputLayers)[1] = "feature_fusion/concat_3";
}

cv::Mat TextDetection::apply(const cv::Mat& image)
{
    std::vector<cv::Mat> output;
    cv::dnn::blobFromImage(image, blob, 1.0, cv::Size(m_width, m_height), cv::Scalar(123.68, 116.78, 103.94), true, false);
    m_net.setInput(blob);
    m_net.forward(output, *m_outputLayers);

    cv::Mat scores = output[0];
    cv::Mat geometry = output[1];

    std::vector<cv::RotatedRect> boxes;
    std::vector<float> confidences;
    decode(scores, geometry, m_confidenceThreshold, boxes, confidences);

    std::vector<int> indices;
    cv::dnn::NMSBoxes(boxes, confidences, m_confidenceThreshold, n_nmsThreshold, indices);

    cv::Point2f ratio((float)image.cols / m_width, (float)image.rows / m_height);
    for (size_t i = 0; i < indices.size(); ++i) {
        cv::RotatedRect& box = boxes[indices[i]];

        cv::Point2f vertices[4];
        box.points(vertices);

        for (int j = 0; j < 4; ++j) {
            vertices[j].x *= ratio.x;
            vertices[j].y *= ratio.y;
        }

        for (int j = 0; j < 4; ++j)
            cv::line(image, vertices[j], vertices[(j + 1) % 4], cv::Scalar(0, 255, 0), 2, cv::LINE_AA);
    }

    std::vector<double> layersTimes;
    double freq = cv::getTickFrequency() / 1000;
    double t = m_net.getPerfProfile(layersTimes) / freq;
    std::string label = cv::format("Inference time: %.2f ms", t);
    cv::putText(image, label, cv::Point(0, 15), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 255, 0));

    return image;
}

void TextDetection::decode(const cv::Mat &scores, const cv::Mat &geometry, float scoreThreshold, std::vector<cv::RotatedRect> &detections, std::vector<float> &confidences)
{
    detections.clear();

    const int height = scores.size[2];
    const int width = scores.size[3];
    for (int y = 0; y < height; ++y) {

        const float* scoresData = scores.ptr<float>(0, 0, y);
        const float* x0_data = geometry.ptr<float>(0, 0, y);
        const float* x1_data = geometry.ptr<float>(0, 1, y);
        const float* x2_data = geometry.ptr<float>(0, 2, y);
        const float* x3_data = geometry.ptr<float>(0, 3, y);
        const float* anglesData = geometry.ptr<float>(0, 4, y);

        for (int x = 0; x < width; ++x) {
            float score = scoresData[x];

            if (score < scoreThreshold) continue;

            float offsetX = x * 4.0f;
            float offsetY = y * 4.0f;
            float angle = anglesData[x];
            float cosA = std::cos(angle);
            float sinA = std::sin(angle);
            float h = x0_data[x] + x2_data[x];
            float w = x1_data[x] + x3_data[x];

            cv::Point2f offset(offsetX + cosA * x1_data[x] + sinA * x2_data[x],
                              offsetY - sinA * x1_data[x] + cosA * x2_data[x]);

            cv::Point2f p1 = cv::Point2f(-sinA * h, -cosA * h) + offset;
            cv::Point2f p3 = cv::Point2f(-cosA * w, sinA * w) + offset;

            cv::RotatedRect r(0.5f * (p1 + p3), cv::Size2f(w, h), -angle * 180.0f / (float)CV_PI);

            detections.push_back(r);
            confidences.push_back(score);
        }
    }
}
