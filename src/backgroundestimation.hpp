#ifndef BACKGROUNDESTIMATION_HPP
#define BACKGROUNDESTIMATION_HPP

#include "basealgorithm.hpp"

class BackgroundEstimation : public BaseAlgorithm
{
public:
    BackgroundEstimation();

    cv::Mat apply(const cv::Mat&);

private:
    int computeMedian(std::vector<int> elements);
    cv::Mat _computeMedian(std::vector<cv::Mat> vec);

};

#endif // BACKGROUNDESTIMATION_HPP
