#ifndef EXPOSUREFUSION_HPP
#define EXPOSUREFUSION_HPP

#include "basealgorithm.hpp"

class ExposureFusion : public BaseAlgorithm
{
public:
    ExposureFusion(const std::vector<std::string>& fileNames);
    cv::Mat apply(const cv::Mat&);

private:
    std::vector<cv::Mat> m_images;
};

#endif // EXPOSUREFUSION_HPP
