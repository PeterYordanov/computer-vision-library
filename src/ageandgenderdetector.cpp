#include "ageandgenderdetector.hpp"

AgeAndGenderDetector::AgeAndGenderDetector()
{
    std::string faceProto = "deps/models/opencv_face_detector.pbtxt";
    std::string faceModel = "deps/models/opencv_face_detector_uint8.pb";

    std::string ageProto = "deps/models/age_deploy.prototxt";
    std::string ageModel = "deps/models/age_net.caffemodel";

    std::string genderProto = "deps/models/gender_deploy.prototxt";
    std::string genderModel = "deps/models/gender_net.caffemodel";

    m_ageList = { "(0-2)", "(4-6)",
                  "(8-12)", "(15-20)",
                  "(25-32)", "(38-43)",
                  "(48-53)", "(60-100)" };

    m_genderList = { "Male", "Female" };

    m_ageNet = cv::dnn::readNet(ageModel, ageProto);
    m_genderNet = cv::dnn::readNet(genderModel, genderProto);
    m_faceNet = cv::dnn::readNet(faceModel, faceProto);

}

cv::Mat AgeAndGenderDetector::apply(const cv::Mat& image)
{
    cv::Scalar MODEL_MEAN_VALUES = cv::Scalar(78.4263377603, 87.7689143744, 114.895847746);
    int padding = 20;

    std::vector<std::vector<int>> boundingBoxes;
    cv::Mat frameFace;
    std::tie(frameFace, boundingBoxes) = getFaceBox(m_faceNet, const_cast<cv::Mat&>(image), 0.7);

    if(boundingBoxes.size() == 0) {
        std::cout << "No face detected" << std::endl;
        return image;
    }

    for (auto& item : boundingBoxes) {
        cv::Rect rec(item.at(0) - padding,
                     item.at(1) - padding,
                     item.at(2) - item.at(0) + 2 * padding,
                     item.at(3) - item.at(1) + 2 * padding);

        cv::Mat face = image(rec);

        cv::Mat blob = cv::dnn::blobFromImage(face, 1, cv::Size(227, 227), MODEL_MEAN_VALUES, false);

        m_genderNet.setInput(blob);
        m_ageNet.setInput(blob);

        std::vector<float> genderPreds = m_genderNet.forward();
        std::vector<float> agePreds = m_ageNet.forward();

        int max_indice_age = std::distance(agePreds.begin(), max_element(agePreds.begin(), agePreds.end()));
        int max_index_gender = std::distance(genderPreds.begin(), max_element(genderPreds.begin(), genderPreds.end()));

        std::string label = m_genderList[max_index_gender] + ", " + m_ageList[max_indice_age];
        cv::putText(frameFace, label, cv::Point(item.at(0), item.at(1) -15),
                    cv::FONT_HERSHEY_SIMPLEX, 0.9, cv::Scalar(0, 255, 0), 2, cv::LINE_AA);
    }

    return frameFace;
}


std::tuple<cv::Mat, std::vector<std::vector<int>>>
AgeAndGenderDetector::getFaceBox(cv::dnn::Net net, cv::Mat& frame, double confidenceThreshold)
{
    cv::Mat frameOpenCVDNN = frame.clone();
    int frameHeight = frameOpenCVDNN.rows;
    int frameWidth = frameOpenCVDNN.cols;
    double inScaleFactor = 1.0;
    cv::Size size = cv::Size(300, 300);
    cv::Scalar meanVal = cv::Scalar(104, 117, 123);

    cv::Mat inputBlob;
    cv::dnn::blobFromImage(frameOpenCVDNN, inputBlob, inScaleFactor, size, meanVal, true, false);

    net.setInput(inputBlob, "data");
    cv::Mat detection = net.forward("detection_out");

    cv::Mat detectionMat(detection.size[2], detection.size[3], CV_32F, detection.ptr<float>());

    std::vector<std::vector<int>> boundingBoxes;

    for(int i = 0; i < detectionMat.rows; i++) {
        float confidence = detectionMat.at<float>(i, 2);

        if(confidence > confidenceThreshold) {
            int x1 = static_cast<int>(detectionMat.at<float>(i, 3) * frameWidth);
            int y1 = static_cast<int>(detectionMat.at<float>(i, 4) * frameHeight);
            int x2 = static_cast<int>(detectionMat.at<float>(i, 5) * frameWidth);
            int y2 = static_cast<int>(detectionMat.at<float>(i, 6) * frameHeight);
            std::vector<int> box = { x1, y1, x2, y2 };
            boundingBoxes.push_back(box);
            //cv::rectangle(frameOpenCVDNN, cv::Point(x1, y1), cv::Point(x2, y2), cv::Scalar(0, 255, 0), 2, 4);
        }
    }

    return std::make_tuple(frameOpenCVDNN, boundingBoxes);
}

