#ifndef AGEANDGENDERDETECTOR_HPP
#define AGEANDGENDERDETECTOR_HPP

#include "basealgorithm.hpp"

class AgeAndGenderDetector : public BaseAlgorithm
{
public:
    AgeAndGenderDetector();
    cv::Mat apply(const cv::Mat&);

private:
    std::tuple<cv::Mat, std::vector<std::vector<int>>>
    getFaceBox(cv::dnn::Net net, cv::Mat& frame, double confidenceThreshold);

    std::vector<std::string> m_ageList;
    std::vector<std::string> m_genderList;

    cv::dnn::Net m_ageNet;
    cv::dnn::Net m_genderNet;
    cv::dnn::Net m_faceNet;

};

#endif // AGEANDGENDERDETECTOR_HPP
