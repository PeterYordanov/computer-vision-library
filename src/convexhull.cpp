#include "convexhull.hpp"

cv::Mat ConvexHull::apply(const cv::Mat& image)
{
    cv::Mat gray;
    cv::Mat blur_image;
    cv::Mat threshold_output;

    cv::cvtColor(image, gray, cv::COLOR_BGR2GRAY);
    cv::blur(gray, blur_image, cv::Size(3, 3));
    cv::threshold(gray, threshold_output, 200, 255, cv::THRESH_BINARY);

    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;

    cv::findContours(threshold_output,
                     contours,
                     hierarchy,
                     cv::RETR_TREE,
                     cv::CHAIN_APPROX_SIMPLE,
                     cv::Point(0, 0));

    std::vector<std::vector<cv::Point>> hull(contours.size());

    for(int i = 0; i < contours.size(); i++)
        cv::convexHull(cv::Mat(contours[i]), hull[i], false);

    cv::Mat drawing = cv::Mat::zeros(threshold_output.size(), CV_8UC3);

    for(int i = 0; i < contours.size(); i++) {
        cv::Scalar color_contours = cv::Scalar(0, 255, 0);
        cv::Scalar color = cv::Scalar(255, 255, 255);

        cv::drawContours(drawing,
                         contours,
                         i,
                         color_contours,
                         2,
                         8,
                         std::vector<cv::Vec4i>(),
                         0,
                         cv::Point());

        cv::drawContours(drawing,
                         hull,
                         i,
                         color,
                         2,
                         8,
                         std::vector<cv::Vec4i>(),
                         0,
                         cv::Point());
    }

    return drawing;
}
