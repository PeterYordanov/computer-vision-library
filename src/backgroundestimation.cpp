#include "backgroundestimation.hpp"

BackgroundEstimation::BackgroundEstimation()
{

}

cv::Mat BackgroundEstimation::apply(const cv::Mat& image)
{
    cv::Mat grayMedianFrame;
    cv::Mat medianFrame = _computeMedian(std::vector<cv::Mat>(image));
    cv::cvtColor(medianFrame, grayMedianFrame, cv::COLOR_BGR2GRAY);
    cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
    cv::Mat dframe;
    cv::absdiff(image, grayMedianFrame, dframe);
    cv::threshold(dframe, dframe, 30, 255, cv::THRESH_BINARY);

    return dframe;
}

int BackgroundEstimation::computeMedian(std::vector<int> elements) {
    std::nth_element(elements.begin(), elements.begin() + elements.size() / 2, elements.end());

    return elements[elements.size() / 2];
}

cv::Mat BackgroundEstimation::_computeMedian(std::vector<cv::Mat> vec) {
    cv::Mat medianImage(vec[0].rows, vec[0].cols, CV_8UC3, cv::Scalar(0, 0, 0));

    for(int row = 0; row < vec[0].rows; row++) {
        for(int col = 0; col < vec[0].cols; col++) {
            std::vector<int> elements_B;
            std::vector<int> elements_G;
            std::vector<int> elements_R;

            for(int imgNumber=0; imgNumber < vec.size(); imgNumber++) {
                int B = vec[imgNumber].at<cv::Vec3b>(row, col)[0];
                int G = vec[imgNumber].at<cv::Vec3b>(row, col)[1];
                int R = vec[imgNumber].at<cv::Vec3b>(row, col)[2];

                elements_B.push_back(B);
                elements_G.push_back(G);
                elements_R.push_back(R);
            }

            medianImage.at<cv::Vec3b>(row, col)[0] = computeMedian(elements_B);
            medianImage.at<cv::Vec3b>(row, col)[1] = computeMedian(elements_G);
            medianImage.at<cv::Vec3b>(row, col)[2] = computeMedian(elements_R);
        }
    }

    return medianImage;
}
